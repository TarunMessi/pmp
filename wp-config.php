<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pmp');

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '?1`2RV^A0vx{SUGH[1rPyvyc+-]Or*|jocff`T!85Mj0w%zw.Iw]$LV#a6<<4N*7' );
define( 'SECURE_AUTH_KEY',  '?|9}jEbL#.w7#O|lAp~.LX0eA_|?2Zkl(q>TVDA]9&AS3$DRTcr>]s <;_4r2^xC' );
define( 'LOGGED_IN_KEY',    'G!L^1)2M^:0#[wSeeiKO4j)-AT=NCorsl)`_u;j@X4D>=y~2ZDIm_)+q:m i#Q/d' );
define( 'NONCE_KEY',        'l.<pOZ08I]9f8nkZx}{>9kA~n_z{pW#XO2=f1r1~tgDWVlTz94U{dOn$}:H+nwyj' );
define( 'AUTH_SALT',        'soIaVnG%*cZ*g%94Q~5J>ox&d?NGs$}-_>/H4h#0S=Tr^^}z:N!i:[<e,fy1{8FH' );
define( 'SECURE_AUTH_SALT', '$*vyn/LU!m!;GOtiV+{7UK`}AJ_;%NZrAvn?Fd%j8Znf#y`?,9ucil+JZ^j1b5jM' );
define( 'LOGGED_IN_SALT',   '.]u+(2v)D.NROUDtCqvDKg(W/w9osqbBjM7s7k3&Q`M7,65aGIjKlp6C#^h%0ykF' );
define( 'NONCE_SALT',       '43v1zNn!b;&TxA-qaVfr*|7r1Tyn<%r9?!MQXn8O,@5`6sD<NNiz>kfBjpK(mHrs' );

define('JWT_AUTH_SECRET_KEY', 'nz.64QXW$z`.~uHa-S?|r6^JT-{xW)kd A:buLQnMr%<U#K.X3raUn|4lGDCR]6u');
define('JWT_AUTH_CORS_ENABLE', true);
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
