jQuery(function() {
    window.MOKO_NOTIFICATION_URL = 'https://moko-notification-server.herokuapp.com/notifications';
    window.MOKO_API_KEY = 'PG4YZ8S59B4T61HBFTS2JY297VXC';

    // set current date
    var currDate = new Date();
    jQuery('#notification_send_at_1i').val(currDate.getFullYear());
    jQuery('#notification_send_at_2i').val(currDate.getMonth()+1);
    jQuery('#notification_send_at_3i').val(currDate.getDate());
    var hours = currDate.getHours().toString();
    if (hours.length == 1) hours = "0" + hours;
    jQuery('#notification_send_at_4i').val(hours);
    var minutes = currDate.getMinutes().toString();
    if (minutes.length == 1) minutes = "0" + minutes;
    jQuery('#notification_send_at_5i').val(minutes);

    jQuery('#push_now input').change(function(e) {
        if (jQuery(e.target).is(':checked')) {
            jQuery('#push_send_at').css('opacity', 0.3);
            return jQuery('#push_send_at select').attr('disabled', true);
        } else {
            jQuery('#push_send_at').css('opacity', 1);
            return jQuery('#push_send_at select').attr('disabled', false);
        }
    });

    jQuery("#notification_message").on("input", function(e) {
        var val = jQuery(e.target).val();
        var carriageReturns = val.split("\n").length - 1;
        var remainingChars = 107 - val.length - carriageReturns;
        jQuery('#msg_chars_left span').text(remainingChars);
    });

    function getTags() {
        var tags = [];
        jQuery('.notification-tag:checked').each(function (idx, el) {
            tags.push(jQuery(el).attr('data-tag'));
        });
        return JSON.stringify(tags);
    }

    function addScheduleParams(dataPayload) {
        if (!jQuery('#push_now_flag').is(':checked')) {
            var day                                  = jQuery('#notification_send_at_3i').val();
            var month                                = jQuery('#notification_send_at_2i').val();
            var year                                 = jQuery('#notification_send_at_1i').val();
            var hour                                 = jQuery('#notification_send_at_4i').val();
            var minute                               = jQuery('#notification_send_at_5i').val();
            var dateStr                              = year + '-' + month + '-' + day + ' ' + hour + ':' + minute;
            var date                                 = new Date(Date.parse(dateStr));
            dataPayload['date'] = date;
        }
        else{
            dataPayload['date'] = '';
        }
    }

    // http://stackoverflow.com/questions/7445328/check-if-a-string-is-a-date-value
    function isValidDate(s) {
        var separators = ['\\.', '\\-', '\\/'];
        var bits = s.split(new RegExp(separators.join('|'), 'g'));
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d.getFullYear() == bits[2] && d.getMonth() + 1 == bits[1];
    }

    function isScheduleValid() {
        if (jQuery('#push_now_flag').length > 0 && !jQuery('#push_now_flag').is(':checked')) {
            var day = jQuery('#notification_send_at_3i').val();
            var month = jQuery('#notification_send_at_2i').val();
            var year = jQuery('#notification_send_at_1i').val();
            var hour = jQuery('#notification_send_at_4i').val();
            var minute = jQuery('#notification_send_at_5i').val();
            var dateStr = year + '-' + month + '-' + day + ' ' + hour + ':' + minute;
            return isValidDate(day + '-' + month + '-' + year) && (Date.parse(dateStr) > (new Date()));
        }
        return true;
    }

    jQuery("#push_notifier_btn").on("click", function() {
        jQuery("#push_notifier_btn").attr('disabled', true);
        var msg = jQuery("#notification_message").val();
        var errorMsg = "Message must not be blank - please enter a message to send";
        if (msg.length <= 0) {
            jQuery("#notification_notice").css('color','red').text(errorMsg);
            jQuery("#push_notifier_btn").attr('disabled', false);
        } else if (!isScheduleValid()) {
            jQuery("#notification_notice").css('color','red').text("Date specified is not valid - please enter a valid date");
            jQuery("#push_notifier_btn").attr('disabled', false);
        }else {
            console.log("time to send '"+msg+"'");
            var dataPayload = {
                'message': msg,
                'tags': getTags()
            };
            addScheduleParams(dataPayload);
            jQuery.ajax({
                url: window.MOKO_NOTIFICATION_URL,
                type: 'POST',
                data: dataPayload,
                dataType: 'json',
                crossDomain: true,
                cache: false,
                headers: {
                    'X-Api-Key':window.MOKO_API_KEY,
                },
                success: function (data) {
                    jQuery("#notification_message").val("");
                    jQuery('#msg_chars_left span').text(107);
                    if (jQuery('#push_now_flag').is(':checked')) {
                        jQuery("#notification_notice").css('color','green').text("Message was successfully sent! (delivery may take up to a minute)");
                    } else {
                        jQuery("#notification_notice").css('color','green').text("Message was successfully scheduled.");
                    }
                    jQuery("#push_notifier_btn").attr('disabled', false);
                    console.log("success!");
                    console.log(data);
                },
                error: function (data) {
                    jQuery("#push_notifier_btn").attr('disabled', false);
                    jQuery("#notification_notice").css('color','red').text("ERROR sending message - please contact support@mokoapp.com");
                    console.log("error!");
                    console.log(data);
                }
            });

        }
    });
});