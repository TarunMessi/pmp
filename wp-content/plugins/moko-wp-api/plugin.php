<?php
/**
 * Plugin Name: Moko API
 * Plugin URI: http://mokoapp.com
 * Description: WP REST API extensions to curate content for the Moko phone app.
 * Author: Rajesh kumar at Moko
 * Author URI: mailto:support@mokoapp.com
 * Version: 1.0
 * License: GPL2+
 */

$role = add_role( 'carisbrooke_admin', 'Carisbrooke Admin', array(
  'read' => true, // True allows that capability
  'edit_posts' => true,
  'publish_posts' => true,
  'delete_posts' => true,
  'read_private_posts' => true,
  'edit_private_posts' => true,
  'delete_private_posts' => true,
  'delete_published_posts' => true,
  'edit_published_posts' => true,
  'edit_others_posts' => true,
  'delete_others_posts' => true,
  'manage_categories' => true,
  'upload_files' => true,
  'edit_tribe_event' => true,
  'read_tribe_event' => true,
  'delete_tribe_event' => true,
  'delete_tribe_events' => true,
  'edit_tribe_events' => true,
  'edit_others_tribe_events' => true,
  'delete_others_tribe_events' => true,
  'publish_tribe_events' => true,
  'edit_published_tribe_events' => true,
  'delete_published_tribe_events' => true,
  'delete_private_tribe_events' => true,
  'edit_private_tribe_events'=> true,
  'read_private_tribe_events' => true,
  'edit_tribe_venue' => true,
  'read_tribe_venue' => true,
  'delete_tribe_venue' => true,
  'delete_tribe_venues' => true,
  'edit_tribe_venues' => true,
  'edit_others_tribe_venues' => true,
  'delete_others_tribe_venues' => true,
  'publish_tribe_venues' => true,
  'edit_published_tribe_venues' => true,
  'delete_published_tribe_venues' => true,
  'delete_private_tribe_venues' => true,
  'edit_private_tribe_venues' => true,
  'read_private_tribe_venues' => true,
  'edit_tribe_organizer' => true,
  'read_tribe_organizer' => true,
  'delete_tribe_organizer' => true,
  'delete_tribe_organizers' => true,
  'edit_tribe_organizers' => true,
  'edit_others_tribe_organizers' => true,
  'delete_others_tribe_organizers' => true,
  'publish_tribe_organizers' => true,
  'edit_published_tribe_organizers' => true,
  'delete_published_tribe_organizers' => true,
  'delete_private_tribe_organizers' => true,
  'edit_private_tribe_organizers' => true,
  'read_private_tribe_organizers' => true
  ) );
  
function wd_admin_menu_rename() {
  global $menu; // Global to get menu array
  $menu[5][0] = 'Announcements'; 
  $menu[6][0] = 'Calendar';
}
add_action( 'admin_menu', 'wd_admin_menu_rename' );

function remove_menu(){
  // remove_menu_page('edit.php');//removing posts
  $user = wp_get_current_user();
  if ( in_array( 'carisbrooke_admin', (array) $user->roles ) ) {
    remove_menu_page('tools.php'); // Tools
    remove_menu_page('edit-comments.php'); // Comments
    remove_menu_page( 'upload.php' ); // Media
  }
  remove_menu_page( 'edit.php' ); //Posts
  }

add_action( 'admin_menu', 'remove_menu' );

add_action( 'init', 'moko_custom_post_type_rest_support', 25 );

function moko_custom_post_type_rest_support() {

  global $wp_post_types;

  $post_type_name = 'post';

  if ( isset( $wp_post_types[ $post_type_name ] ) ) {

    $wp_post_types[$post_type_name]->show_in_rest = true;
    // Optionally customize the rest_base or controller class
    $wp_post_types[$post_type_name]->rest_base = $post_type_name;
    $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';

  }

}

function moko_get_documents() {
  
  $parent_term = get_terms(array(
    'taxonomy' => 'documents_categories',
    'hide_empty'    => false
  ));
  
  $documents = [];
  
  $totalArryElem = count($parent_term);
  
  for ($x = 0; $x < $totalArryElem; $x++) {
    $posts = get_posts(
      array(
        'post_type' => 'documents',
        'posts_per_page'=>-1, 
        'numberposts'=>-1,
        'tax_query' => array(
            array(
              'taxonomy'=> $parent_term[$x]->taxonomy,
              'field'=> $parent_term[$x]->term_id,
              'terms'=>  $parent_term[$x]->term_taxonomy_id,
          ),
        ),
      )
    );

    $countPosts = count($posts);
    
    for ($y = 0; $y < $countPosts; $y++) {
      $fileDetails = get_field('upload_document', $posts[$y]->ID);

      $template = new stdClass;
      $template->category = $parent_term[$x]->name;
      $template->id = $posts[$y]->ID;
      $template->title = $posts[$y]->post_title;
      $template->url = $fileDetails['url'];


      array_push($documents, $template);
    }

  }

	return [ 'documents' => $documents];
}

function moko_get_our_announcements() {

  $posts = get_posts(
    array(
      'post_type' => 'announcements',
      'posts_per_page'=>-1, 
      'numberposts'=>-1,
    )
  );

	return [ 'announcements' => $posts ];

}

function moko_get_newsletters() {
  
  $parent_term = get_terms(array(
    'taxonomy' => 'newsletter_groups',
    'hide_empty'    => false
  ));
  
  $newsletters = [];
  
  $totalArryElem = count($parent_term);
  
  for ($x = 0; $x < $totalArryElem; $x++) {
    $posts = get_posts(
      array(
        'post_type' => 'newsletters',
        'posts_per_page'=>-1, 
        'numberposts'=>-1,
        'tax_query' => array(
            array(
              'taxonomy'=> $parent_term[$x]->taxonomy,
              'field'=> $parent_term[$x]->term_id,
              'terms'=>  $parent_term[$x]->term_taxonomy_id,
          ),
        ),
      )
    );

    $countPosts = count($posts);
    
    for ($y = 0; $y < $countPosts; $y++) {
      $fileDetails = get_field('newsletter_document', $posts[$y]->ID);

      $template = new stdClass;
      $template->category = $parent_term[$x]->name;
      $template->id = $posts[$y]->ID;
      $template->title = $posts[$y]->post_title;
      $template->url = $fileDetails['url'];
      $template->posted = $posts[$y]->post_date;

      array_push($newsletters, $template);
    }

  }

  return [ 'newsletters' => $newsletters];
}

function moko_get_our_carouselimg() {
  $imageUrl = [];
  
  $posts = get_posts(
    array(
      'post_type' => 'carousel_images',
     )
  );
  
  $countPosts = count($posts);

  if($countPosts > 3){
    $countPosts = 3;
  }

  for ($y = 0; $y < $countPosts; $y++) {
    $meta = get_post_meta($posts[$y]->ID,'', true);
    $url = wp_get_attachment_url($meta['upload_image'][0]);

    array_push($imageUrl, $url);
  }
  
  return $imageUrl;
}

function moko_get_contact_number() {
  $parent_term = get_terms(array(
    'taxonomy' => 'useful_contact_numbers',
    'hide_empty'    => false
  ));
  
  $UsefulNumbers = [];
  
  $totalArryElem = count($parent_term);
  
  for ($x = 0; $x < $totalArryElem; $x++) {
    $posts = get_posts(
      array(
        'post_type' => 'usefull_contact_num',
        'posts_per_page'=>-1, 
        'numberposts'=>-1,
        'tax_query' => array(
            array(
              'taxonomy'=> $parent_term[$x]->taxonomy,
              'field'=> $parent_term[$x]->term_id,
              'terms'=>  $parent_term[$x]->term_taxonomy_id,
          ),
        ),
      )
    );

    $countPosts = count($posts);
    
    for ($y = 0; $y < $countPosts; $y++) {
      $meta = get_post_meta($posts[$y]->ID,'', true);

      $template = new stdClass;
      $template->title = $parent_term[$x]->name;
      $template->name = $meta['name'][0];
      $template->number = $meta['contact_number'][0];

      array_push($UsefulNumbers, $template);
    }

  }

	return [ 'contact_numbers' => $UsefulNumbers];
}

add_action( 'rest_api_init', 'moko_register_api_routes' );

function moko_register_api_routes() {
	register_rest_route( 'moko/v1', '/documents', array(
		'methods' => 'GET',
		'callback' => 'moko_get_documents',
	) );

	register_rest_route( 'moko/v1', '/announcements', array(
		'methods' => 'GET',
		'callback' => 'moko_get_our_announcements',
	) );
  
  register_rest_route( 'moko/v1', '/newsletters', array(
		'methods' => 'GET',
		'callback' => 'moko_get_newsletters',
  ) );
  
  register_rest_route( 'moko/v1', '/contact_numbers', array(
		'methods' => 'GET',
		'callback' => 'moko_get_contact_number',
  ) );
  
  register_rest_route( 'moko/v1', '/carouselimg', array(
    'methods' => 'GET',
    'callback' => 'moko_get_our_carouselimg',
  ) );
}